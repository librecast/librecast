package main

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"

	"gitlab.com/librecast/librecast/internal/db"

	"gitlab.com/librecast/librecast/internal/app"

	"github.com/sirupsen/logrus"

	"gitlab.com/librecast/librecast/internal/services/git"
)

func main() {
	log := logrus.New()
	log.SetLevel(logrus.DebugLevel)

	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGINT, syscall.SIGTERM)

	committer := &git.Signature{Name: "Librecast", Email: "git@libreca.st"}
	svc := git.New(nil, committer, nil)

	bdb, err := db.NewBbolt("librecast.db", nil)
	a := app.New(bdb, svc, time.Second, log.WithField("dev", true))

	recSess, err := a.CreateRecordingSession(app.CreateRecordingSessionInput{
		Name:      "Test Recording",
		WatchPath: "/Users/eli/dev/librecast/librecast/tmp",
	})
	if err != nil {
		panic(err)
	}

	fmt.Printf("Created recording session: %s\n", recSess.ID)

	recSess, err = a.StartRecordingSession(recSess.ID)
	if err != nil {
		panic(err)
	}

	<-sig // wait for ctrl+c

	recSess, err = a.StopRecordingInProgress(recSess.ID)
	if err != nil {
		panic(err)
	}

	fmt.Printf("Done!\n%#v\n", recSess)

	//repo, err := svc.SetupRepository(".", "/tmp/librecast")
	//if err != nil {
	//	panic(err)
	//}
	//
	//w, err := watcher.NewFsNotify()
	//if err != nil {
	//	panic(err)
	//}
	//
	//repoWatcher := repowatcher.New(repo, w, time.Second)
	//repoWatcher.Watch(ctx, logrus.New())
}

/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2023 Brett Sheffield <bacs@librecast.net> */

#ifndef _TESTDATA_H
#define _TESTDATA_H 1

#include "test.h"
#include <fcntl.h>
#include <librecast/types.h>
#include <librecast/crypto.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/param.h>
#include <sys/stat.h>
#include <unistd.h>

enum {
	TEST_TMP = 1,
	TEST_RND = 2,
	TEST_OWN = 4,
	TEST_MOD = 8,
};

/* write sz random bytes to ptr */
int test_random_bytes(void *ptr, size_t sz);

/* create test file with of sz bytes at path filename
 *
 * flags:
 *   TEST_TMP - if set, a temp file is created using filename as the template.
 *   TEST_RND - fill the file with sz random bytes
 *
 * if sz > 0 but TEST_RND is not set, a sparse file is created.
 */
int test_data_file(char *filename, size_t sz, int flags);

/* verify filename is a regular file and is correct size
 * return 0 if correct, -1 on error */
int test_data_size(char *filename, size_t sz);

/* hash file
 *   filename - file to hash
 *   hash - buffer to return hash in
 *   hashlen - length of hash buffer
 *   buf - either NULL or a pointer to a preallocated buffer for file I/O
 *   buflen - length of buf in bytes
 * returns 0 on success, -1 on error, setting errno
 */
int test_hash_file(const char *filename, unsigned char *hash, size_t hashlen, char *buf, size_t buflen);

/* hash files and confirm data matches, return 0 on match, nonzero fail */
int test_file_match(const char *file1, const char *file2);

/* write len random bytes to file, starting at offset off
 * returns 0 on success, -1 on error, setting errno */
int test_file_scratch(const char *filename, long offset, size_t len);

/* extend file sparsely to sz bytes */
int test_sparsify_fd(int fd, size_t sz);

/* randomize file metadata
 * path must be an existing directory
 * flags can be:
 * - TEST_OWN - randomize uid and gid
 * - TEST_MOD - randomize permissions */
int test_random_meta(const char *path, int flags);

/* create a directory tree of files and directories with directories nested
 * depth levels deep, up to max_files files and max_dirs directories in each
 * level. Fill files with up to max_filesz random bytes.
 * path must be an existing directory
 * flags can be:
 * - TEST_OWN - randomize uid and gid
 * - TEST_MOD - randomize permissions */
int test_createtesttree(const char *path, off_t max_filesz, int max_files, int max_dirs, int depth,
		int flags);

/* create a pair of source and destination directories using the template
 * of the form 0000-0000.{src,dst}.tmp.XXXXXX based on the test name.
 * testprog should be passed as basename(argv[0])
 * Both src and dst need to be free()ed after use. */
int test_createtestdirs(char *testprog, char **src, char **dst);

/* create a single test directory as above. srcdst must be "src" or "dst" */
int test_createtestdir(char *testprog, char **dir, const char *srcdst);

#endif /* _TESTDATA_H */

/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2023 Brett Sheffield <bacs@librecast.net> */

#include "test.h"
#include "testdata.h"
#include <librecast/mdex.h>
#include <librecast.h>
#include <librecast/crypto.h>

int main(void)
{
	mdex_t *mdex;
	unsigned char rand[123];
	unsigned char hash[HASHSIZE];
	int rc;

	test_name("mdex_get() find entry with truncated hash");

	mdex = mdex_init(1);
	test_random_bytes(rand, sizeof rand);
	hash_generic(hash, HASHSIZE, rand, sizeof rand);

	rc = mdex_get(mdex, hash, HASHSIZE, NULL);
	test_assert(rc == -1, "entry not found before adding");

	rc = mdex_put(mdex, hash, HASHSIZE, NULL);
	test_assert(rc == 0, "entry added");

	rc = mdex_get(mdex, hash, 14, NULL);
	test_assert(rc == 0, "entry found using truncated hash");

	mdex_free(mdex);
	return test_status;
}

/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2024 Brett Sheffield <bacs@librecast.net> */

/*
 * router start/stop/forwarding
 */

#include "test.h"
#include <librecast_pvt.h>
#include <librecast/router.h>

#define SOCKS 4

int main(void)
{
	lc_ctx_t *lctx;
	lc_router_t *r1;
	lc_socket_t *sock[SOCKS] = {0};
	const unsigned int ports = 2;

	test_name("router_start() / router_stop()");

	/* create context and router */
	lctx = lc_ctx_new();
	if (!test_assert(lctx != NULL, "lc_ctx_new()")) return test_status;
	r1 = lc_router_new(lctx, ports, 0);
	if (!test_assert(r1 != NULL, "lc_router_new()")) goto err_ctx_free;
	test_assert(r1->ports == ports, "router ports set");

	/* create some sockets */
	for (int i = 0; i < SOCKS; i++) {
		sock[i] = lc_socket_new(lctx);
		if (!sock[i]) goto err_socks_free;
	}
	/* plug sockets into router */
	for (int i = 0; i < SOCKS; i++) {
		test_assert(lc_router_socket_add(r1, sock[i]) == 0, "lc_router_socket_add()[%i]", i);
	}
	/* start router */
	if (!test_assert(lc_router_start(r1) == 0, "router_start()")) goto err_socks_free;
	/* stop router */
	if (!test_assert(lc_router_stop(r1) == 0, "router_stop()")) goto err_socks_free;

err_socks_free:
	for (int i = 0; i < SOCKS; i++) {
		if (sock[i]) lc_socket_close(sock[i]);
	}
err_ctx_free:
	lc_ctx_free(lctx);
	return test_status;
}

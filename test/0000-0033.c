/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2021-2024 Brett Sheffield <bacs@librecast.net> */

#include "test.h"
#include <librecast/net.h>
#include <librecast/crypto.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/param.h>

#define CHANNELS 16

int main(void)
{
	lc_ctx_t *lctx;
	lc_channel_t *chan[CHANNELS];
	unsigned char *hash[2];

	test_name("lc_channel_random()");

	lctx = lc_ctx_new();
	test_assert(lctx != NULL, "lc_ctx_new()");
	if (!lctx) return test_status;

	for (int i = 0; i < CHANNELS; i++) {
		chan[i] = lc_channel_random(lctx);
		test_assert(chan[i] != NULL, "error creating channel %i", i);
	}
	for (int i = 0; i < CHANNELS; i++) {
		for (int j = i + 1; j < CHANNELS; j++) {
			hash[0] = lc_channel_get_hash(chan[i]);
			hash[1] = lc_channel_get_hash(chan[j]);
			test_assert(memcmp(hash[0], hash[1], HASHSIZE), "channels must be different");
		}
	}
	lc_ctx_free(lctx);
	return test_status;
}

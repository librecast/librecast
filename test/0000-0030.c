#include "test.h"
#include <librecast/if.h>
#include <sys/types.h>
#include <unistd.h>

int main(void)
{
	char tapname[IFNAMSIZ] = {0};
	int rc;
	test_cap_require(CAP_NET_ADMIN);
	test_name("lc_tap_create()");
	rc = lc_tap_create(tapname);
	if (rc == -1) {
		test_log("lc_tap_create() failed: %s", strerror(errno));
		test_log("tap creation not supported");
		test_assert(errno == ENOTSUP, "lc_tap_create - not supported");
	}
	else {
		test_log("created tap interface %s", tapname);
		test_assert(rc > 0, "lc_tap_create - created");
		test_assert(if_nametoindex(tapname) > 0, "check idx, ensure tap exists");
		close(rc);
	}
	return test_status;
}

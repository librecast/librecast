/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2017-2022 Brett Sheffield <bacs@librecast.net> */

#include "test.h"
#include <librecast/crypto.h>
#include <assert.h>
#include <stdio.h>
#include <unistd.h>

int main(void)
{
	char in[] = "hash browns";
	unsigned char hash[2][HASHSIZE];
	size_t len = strlen(in);
	int key = 1;

	test_name("hash_generic_key()");

	hash_generic_key(hash[0], HASHSIZE, (unsigned char *)in, len, (unsigned char *)&key, sizeof(int));
	hash_generic_key(hash[1], HASHSIZE, (unsigned char *)in, len, (unsigned char *)&key, sizeof(int));
	test_assert(!memcmp(hash[0], hash[1], HASHSIZE), "hashes match");

	/* now change key */
	key++;
	hash_generic_key(hash[1], HASHSIZE, (unsigned char *)in, len, (unsigned char *)&key, sizeof(int));
	test_assert(memcmp(hash[0], hash[1], HASHSIZE), "hashes differ");

	return test_status;
}

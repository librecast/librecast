/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2017-2022 Brett Sheffield <bacs@librecast.net> */

#include "test.h"
#include <librecast/net.h>

int main(void)
{
	lc_ctx_t *lctx;
	lc_channel_t *chan;
	lc_key_t key ={0};
	char *shortkey = "shortkey";
	size_t keylen = strlen(shortkey);
	int rc;

	test_name("Channel key management functions");

	lctx = lc_ctx_new();
	chan = lc_channel_new(lctx, "Librecast");

	rc = lc_channel_setkey(chan, &key, -1);
	test_assert(rc == -1, "lc_channel_setkey() invalid");

	rc = lc_channel_setkey(chan, &key, LC_CODE_SYMM);
	test_assert(rc == 0, "lc_channel_setkey() success");

	rc = lc_channel_set_sym_key(chan, (unsigned char *)shortkey, keylen);
	test_assert(rc == 0, "lc_channel_set_sym_key() success");

	rc = lc_channel_set_pub_key(chan, (unsigned char *)shortkey, keylen);
	test_assert(rc == 0, "lc_channel_set_pub_key() success");

	lc_key_t tmpkey = {0};
	rc = lc_channel_getkey(chan, &tmpkey, LC_CODE_SYMM);
	test_assert(rc == 0, "lc_channel_getkey() success");
	test_expect(shortkey, (char *)tmpkey.key);

	memset(&tmpkey, 0, sizeof tmpkey);
	lc_channel_getkey(chan, &tmpkey, LC_CODE_PUBK);
	test_expect(shortkey, (char *)tmpkey.key);

	lc_ctx_free(lctx);

	return test_status;
}

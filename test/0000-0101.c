/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2021-2023 Brett Sheffield <bacs@librecast.net> */

#include "test.h"
#include <librecast/mtree.h>
#include <errno.h>

int main(void)
{
	size_t node, parent;
	test_name("mtree_parent()");

	node = 0; parent = -1;
	test_assert(mtree_parent(node) == parent,
					"mtree_parent(%zu) => %zu", node, parent);
	node = 1; parent = 0;
	test_assert(mtree_parent(node) == parent,
					"mtree_parent(%zu) => %zu", node, parent);
	node = 2; parent = 0;
	test_assert(mtree_parent(node) == parent,
					"mtree_parent(%zu) => %zu", node, parent);
	node = 3; parent = 1;
	test_assert(mtree_parent(node) == parent,
					"mtree_parent(%zu) => %zu", node, parent);
	node = 4; parent = 1;
	test_assert(mtree_parent(node) == parent,
					"mtree_parent(%zu) => %zu", node, parent);
	node = 5; parent = 2;
	test_assert(mtree_parent(node) == parent,
					"mtree_parent(%zu) => %zu", node, parent);
	node = 6; parent = 2;
	test_assert(mtree_parent(node) == parent,
					"mtree_parent(%zu) => %zu", node, parent);
	node = 7; parent = 3;
	test_assert(mtree_parent(node) == parent,
					"mtree_parent(%zu) => %zu", node, parent);
	node = 16; parent = 7;
	test_assert(mtree_parent(node) == parent,
					"mtree_parent(%zu) => %zu", node, parent);
	return test_status;
}

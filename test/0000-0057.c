/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2017-2022 Brett Sheffield <bacs@librecast.net> */

/* symmetric encryption test - key set on context */

#include "testnet.h"
#include <librecast/crypto.h>
#include <librecast/net.h>
#include <errno.h>
#include <pthread.h>
#include <semaphore.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>

#define WAITS 1

#ifdef HAVE_LIBSODIUM
static sem_t sem;
static ssize_t byt_recv, byt_sent, ohead;
static char channame[] = "0000-0057";
static unsigned char key[crypto_secretbox_KEYBYTES];
static int encryption_on;

void *testthread(void *arg)
{
	lc_ctx_t *lctx;
	lc_socket_t *sock;
	lc_channel_t *chan;

	lctx = lc_ctx_new();
	test_assert(lctx != NULL, "lc_ctx_new()");

	if (encryption_on) {
		lc_ctx_set_sym_key(lctx, key, crypto_secretbox_KEYBYTES);
		lc_ctx_coding_set(lctx, LC_CODE_SYMM);
		ohead = crypto_secretbox_MACBYTES + crypto_secretbox_NONCEBYTES;
	}
	sock = lc_socket_new(lctx);
	test_assert(sock != NULL, "lc_socket_new()");
	chan = lc_channel_new(lctx, channame);
	test_assert(chan != NULL, "lc_channel_new()");
	test_assert(lc_channel_bind(sock, chan) == 0, "lc_channel_bind()");
	test_assert(lc_channel_join(chan) == 0, "lc_channel_join()");

	sem_post(&sem); /* tell send thread we're ready */
	byt_recv = lc_socket_recv(sock, (char *)arg, BUFSIZ, 0);
	if (byt_recv == -1) perror("lc_socket_recv");

	lc_ctx_free(lctx);

	sem_post(&sem); /* tell send thread we're done */

	return arg;
}
#endif

int main(void)
{
#ifndef HAVE_LIBSODIUM
	return test_skip("lc_channel_send() / lc_socket_recv() - symmetric encryption");
#else
	lc_ctx_t *lctx;
	lc_socket_t *sock;
	lc_channel_t *chan;
	pthread_t thread;
	struct timespec ts;
	char buf[] = "liberté";
	char recvbuf[BUFSIZ] = "";

	test_name("lc_channel_send() / lc_socket_recv() - symmetric encryption (ctx key)");
	test_require_net(TEST_NET_BASIC);

	/* fire up test thread */
	sem_init(&sem, 0, 0);
	pthread_create(&thread, NULL, &testthread, &recvbuf);
	sem_wait(&sem); /* recv thread is ready */

	/* Librecast Context, Socket + Channel */
	lctx = lc_ctx_new();
	test_assert(lctx != NULL, "lc_ctx_new()");

	/* generate and set symmetric key on sender */
	crypto_secretbox_keygen(key);
	lc_ctx_set_sym_key(lctx, key, crypto_secretbox_KEYBYTES);
	lc_ctx_coding_set(lctx, LC_CODE_SYMM);

	/* create socket and channel (channel will inherit encoding from ctx) */
	sock = lc_socket_new(lctx);
	test_assert(sock != NULL, "lc_socket_new()");
	chan = lc_channel_new(lctx, channame);
	test_assert(chan != NULL, "lc_channel_new()");
	lc_socket_loop(sock, 1); /* talking to ourselves, set loopback */
	lc_channel_bind(sock, chan);

	/* send msg with PING opcode */
	byt_sent = lc_channel_send(chan, buf, sizeof buf, 0);

	/* wait for recv thread */
	test_assert(!clock_gettime(CLOCK_REALTIME, &ts), "clock_gettime()");
	ts.tv_sec += WAITS;
	test_assert(!sem_timedwait(&sem, &ts), "timeout");
	pthread_cancel(thread);
	pthread_join(thread, NULL);

	test_assert(memcmp(buf, recvbuf, sizeof buf) != 0, "data doesn't match with encryption");

	/* run testthread again with encryption key set */
	encryption_on = 1;
	pthread_create(&thread, NULL, &testthread, &recvbuf);
	sem_wait(&sem); /* recv thread is ready */

	byt_sent = lc_channel_send(chan, buf, sizeof buf, 0);

	/* wait for recv thread */
	test_assert(!clock_gettime(CLOCK_REALTIME, &ts), "clock_gettime()");
	ts.tv_sec += WAITS;
	test_assert(!sem_timedwait(&sem, &ts), "timeout");
	sem_destroy(&sem);

	/* test we can decrypt message */
	if (byt_recv > 0) {
		test_assert(byt_recv == sizeof buf, "received %zi bytes, expected %zu", byt_recv, sizeof buf);
		test_expect(buf, recvbuf);
	}

	test_assert(byt_sent - ohead == byt_recv, "bytes sent (%zi) == bytes received (%zi)",
			byt_sent, byt_recv);

	/* clean up */
	pthread_cancel(thread);
	pthread_join(thread, NULL);
	lc_ctx_free(lctx);

	return test_status;
#endif
}

/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2024 Brett Sheffield <bacs@librecast.net> */

#include "test.h"

int main(void)
{
	char name[] = "testname";
	test_name(name);
	return test_status;
}

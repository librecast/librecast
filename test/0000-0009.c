/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2017-2023 Brett Sheffield <bacs@librecast.net> */

/* testing lc_channel_bind() / lc_channel_unbind() */

#include "test.h"
#include <librecast/net.h>

int main(void)
{
	lc_ctx_t *lctx = NULL;
	lc_socket_t *sock = NULL;
	lc_channel_t *chan = NULL;

	test_name("lc_channel_bind() / lc_channel_unbind()");

	lctx = lc_ctx_new();
	if (!test_assert(lctx != NULL, "lc_ctx_new()")) return test_status;
	sock = lc_socket_new(lctx);
	if (!test_assert(sock != NULL, "lc_socket_new()")) goto err_ctx_free;
	chan = lc_channel_new(lctx, "example.com");
	if (!test_assert(chan != NULL, "lc_channel_new()")) goto err_ctx_free;

	test_assert(lc_channel_bind(sock, chan) == 0,
			"lc_channel_bind returns 0 on success");

	test_assert(lc_channel_socket(chan) == sock,
			"lc_channel_bind() binds channel to socket");

	test_assert(lc_channel_unbind(chan) == 0,
			"lc_channel_unbind() returns 0 on success");

	test_assert(lc_channel_socket(chan) == NULL,
			"lc_channel_unbind() sets chan->socket to NULL");

err_ctx_free:
	lc_ctx_free(lctx);
	return test_status;
}

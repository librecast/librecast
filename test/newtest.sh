#!/bin/sh -ue

SKEL="skel.c"

# first, find filename for new test

for i in `seq -w 0 9999`
do
	TESTEXEC="0000-$i.test"
	TESTFILE="0000-$i.c"
	if [ ! -f ${TESTFILE} ]; then
		break
	fi
done

head -n1 ${SKEL} > ${TESTFILE}
echo "/* Copyright (c) `date +"%Y"` `git config --get user.name` <`git config --get user.email`> */" >> ${TESTFILE}
tail -n+3 ${SKEL} >> ${TESTFILE}

if [ -d all ]; then
	cd all
	ln -s ../${TESTEXEC}
fi

echo ${TESTFILE}

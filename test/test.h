/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2020-2022 Brett Sheffield <bacs@librecast.net> */

#ifndef _TEST_H
#define _TEST_H 1
#ifdef __linux__
# define _GNU_SOURCE
#endif
#include "../src/config.h"
#include <libgen.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include "valgrind.h"
#if HAVE_SYS_ENDIAN_H
# include <sys/endian.h>
#endif
#ifdef __linux__
#include <linux/capability.h>
#else
#define CAP_NET_ADMIN 12
#define CAP_NET_RAW 13
#endif

enum {
	TEST_ERR  = -1,
	TEST_OK   = 0,
	TEST_WARN = 1,
	TEST_FAIL = 2,
	TEST_UNKN = 3
};

extern int test_status;

void test_init(void);
void fail_msg(char *msg, ...);
int test_assert(int condition, char *msg, ...);
int test_assert_q(int condition, char *msg, ...);
int test_strcmp(char *str1, char *str2, char *msg, ...);
int test_strncmp(char *str1, char *str2, size_t len, char *msg, ...);
int test_expect(char *expected, char *got);
int test_expectn(char *expected, char *got, size_t len);
void test_log(char *msg, ...);
void test_name(char *str, ...);
int test_skip(char *str, ...);
void test_cap_require(int cap);
void test_require_linux(void);

#endif /* _TEST_H */

/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2021,2023 Brett Sheffield <bacs@librecast.net> */

/* wrappers for *alloc */
__attribute__((malloc)) __attribute__((alloc_size(1,2)))
void *calloc(size_t nmemb, size_t size);

__attribute__((malloc)) __attribute__((alloc_size(1)))
void *malloc(size_t size);

/* set *alloc calls to force failure with ENOMEM after failafter allocations.
 * Set failafter to -1 to never force fail (default) */
void falloc_setfail(int failafter);

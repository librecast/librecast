/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2022 Brett Sheffield <bacs@librecast.net> */

#include "testnet.h"
#include <mld.h>
#include <mld_pvt.h>

//void mld_query_handler(mld_t *mld, struct msghdr *msg);

int main(void)
{
	mld_t *mld;
	mld_msg_t msg = {0};
	unsigned int ifx;
	int rc;

	test_name("mld_query_msg()");
	test_require_net(TEST_NET_BASIC);

	ifx = get_multicast_if();
	test_assert(ifx, "get_multicast_if() - find multicast capable interface");
	if (!ifx) return TEST_WARN;
	mld = mld_init(0);
	test_assert(mld != NULL, "mld_t allocated");

	rc = mld_query_msg(mld, ifx, NULL, &msg);
	test_assert(rc == 0, "mld_query_msg() returned %i", rc);

	struct icmp6_hdr *icmpv6 = msg.msgh.msg_iov[0].iov_base;
	test_assert(icmpv6->icmp6_type == MLD_LISTENER_QUERY, "icmp6_type = %i",
			icmpv6->icmp6_type);

	// TODO test Querier state
	// TODO win election, test Querier state
	// TODO set ifx via cmsg
	//mld_query_handler(mld, &msg.msgh);

	mld_query_msg_free(&msg);
	mld_free(mld);
	return test_status;
}

# Librecast Release Process

Releases are prepared from the `main` git branch.

The `testing` branch is then rebased from this and a Release Candidate is
prepared (eg. 0.6-RC1).

## Update CHANGELOG.md

- Check `git log` and add anything relevant for release.
- Replace Unreleased with version number and date.

## configure

- Update version number, copyright year etc. in configure.ac
- run autoconf

## test

Run make test / make check as user and root.

## Rebase testing branch from main

```
git checkout testing
git rebase main
```

## Tag Release Candidate

eg. `git tag -a v0.6-RC1`

## Push RC and tag

## Testing on all supported platforms.

## Coverity testing

## Fixes

If fixes required, push another RC and repeat.

## When all testing passes, tag the release

## Rebase release branch

```
git checkout release
git rebase main
```

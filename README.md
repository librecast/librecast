# Librecast

![header](./header.png)

Free, open-source, code screencasting software.

## Goals

- Record code changes as user is recording
- Record video and audio, sync with code changes
- Output a recording in a format that can be read by humans and machines[*](#output-format)
- Be extensible with plugins
- Optionally, transcribe audio and sync text + code + video

## Non-goals

- Replace recording software like [OBS](https://obsproject.com/)
- Replace video editing software
- Support live streaming (could be implemented as a plugin, but not officially supported)

## Output format

The output format will be a zip file or tar.gz that contains a json file of the recording and a directory containing the video recording.
The json file will have metadata about the recording session as well as important timestamps to synchronize the code changes and video. 

## High level architecture

- Backend: Go
  - `bbolt` for the db
  - `fsnotify` for getting file changes
- Frontend: Svelte

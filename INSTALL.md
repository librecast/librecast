# INSTALL

## Prerequisites

None.

## Installing from source

The code compiles using either gcc or clang.  There is a `make clang` target to
force compilation with clang.  The default is whatever your default CC is set
to.

NB: GNU Make is required. On \*BSD install and use gmake. The bash shell is also required for `make test`.

Download the source:

`git clone https://codeberg.org/librecast/librecast.git`

then, do the usual:
```
cd librecast
./configure
make
make install # requires superuser (root/sudo)
```

NB: On Linux you may want to run `ldconfig` (as root) after installing to rebuild your
library cache.

You can set optimization flags for your platform by passing them to configure.
eg.:

```
./configure CFLAGS="-g -O3 -Wall -Wextra -pedantic -march=native -mpopcnt"
```

To install to a different location:

`DESTDIR=/tmp make install`

## Uninstall

`make uninstall` # requires superuser (root/sudo)

## Configure Options

Librecast ships with BLAKE3 included. By default BLAKE3 is used as the hash
function. This allows Librecast to build with no external dependencies.

Provided you have libsodium installed you can use BLAKE2B from sodium instead.
To disable blake3, configure with `--without-blake3`.  If both BLAKE3 and
BLAKE2B are enabled, BLAKE3 is used in preference.

Libsodium is also used for encryption, if available. To explicitly *disable*
libsodium (and all encryption capabilities), configure with `--without-sodium`.

LibLCRQ for FEC will be enabled automatically, if found.  To build without it,
use `--without-lcrq`.

NB: if you build with both of `--without-blake3 --without-sodium` librecast will
not be able to create hashed channels, or do any other hashing. The tests will
not run in this configuration.

libMLD is built into liblibrecast by default. If you do *not* want to include
the MLD API, disable by configuring with --without-mld.

### Install libsodium on Ubuntu

`sudo apt install libsodium-dev`

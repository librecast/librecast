package format

import (
	"github.com/leaanthony/mewn"
	"github.com/xeipuuv/gojsonschema"
)

func Validate(in interface{}) (*gojsonschema.Result, error) {
	schemaStr := mewn.String("./internal/format/schema.json")
	loader, err := gojsonschema.NewSchema(gojsonschema.NewStringLoader(schemaStr))
	if err != nil {
		return nil, err
	}

	return loader.Validate(gojsonschema.NewGoLoader(in))
}

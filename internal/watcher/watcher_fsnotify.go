package watcher

import (
	"context"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"sync"

	"github.com/fsnotify/fsnotify"
	"github.com/monochromegane/go-gitignore"
)

type FsNotify struct {
	watcher *fsnotify.Watcher
	evCh    chan Event
	errCh   chan error
	ignore  gitignore.IgnoreMatcher

	closeOnce sync.Once
}

func NewFsNotify() (*FsNotify, error) {
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		return nil, err
	}
	return &FsNotify{
		watcher: watcher,
		evCh:    make(chan Event),
		errCh:   make(chan error),
	}, nil
}

func (f *FsNotify) Watch(ctx context.Context, path string) (<-chan Event, <-chan error) {
	gitIgnorePath := filepath.Join(path, ".gitignore")
	if _, err := os.Stat(gitIgnorePath); err == nil {
		ignore, err := gitignore.NewGitIgnore(gitIgnorePath)
		if err != nil {
			f.errCh <- err
		}
		f.ignore = ignore
	}

	go f.watch(ctx)
	if err := f.add(path); err != nil {
		f.errCh <- err
	}
	return f.evCh, f.errCh
}

func (f *FsNotify) Close() error {
	if err := f.watcher.Close(); err != nil {
		return err
	}

	f.closeOnce.Do(func() {
		// might already be closed by cancelled context
		close(f.errCh)
		close(f.evCh)
	})

	return nil
}

func (f *FsNotify) add(path string) error {
	// recursively add subdirectories
	return filepath.Walk(path, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if info.IsDir() {
			if !f.isIgnored(path, true) && !strings.HasPrefix(path, ".git") {
				return f.watcher.Add(path)
			}
		}
		return nil
	})
}

func (f *FsNotify) isIgnored(path string, isDir bool) bool {
	if f.ignore == nil {
		return false
	}
	return f.ignore.Match(path, isDir)
}

func (f *FsNotify) watch(ctx context.Context) {
	defer func() {
		f.closeOnce.Do(func() {
			close(f.errCh)
			close(f.evCh)
		})
	}()

	for {
		select {
		case event, ok := <-f.watcher.Events:
			if !ok {
				return
			}

			info, err := os.Stat(event.Name)
			if err != nil && !os.IsNotExist(err) {
				f.errCh <- err
			} else if os.IsNotExist(err) {
				f.evCh <- Event{
					Path: event.Name,
					Info: nil,
					Type: EventType(event.Op),
				}
			} else {
				var raw []byte

				if !info.IsDir() {
					content, err := ioutil.ReadFile(event.Name)
					if err != nil {
						f.errCh <- err
					} else {
						raw = content
					}
				}

				f.evCh <- Event{
					Path:     event.Name,
					Info:     info,
					Type:     EventType(event.Op),
					FileData: raw,
				}
			}
		case err, ok := <-f.watcher.Errors:
			if !ok {
				return
			}
			f.errCh <- err
		case <-ctx.Done():
			return
		}
	}
}

var _ Watcher = (*FsNotify)(nil)

package watcher

import (
	"context"
	"io"
)

// Watcher
type Watcher interface {
	io.Closer
	Watch(ctx context.Context, path string) (<-chan Event, <-chan error)
}

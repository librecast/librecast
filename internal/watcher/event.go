package watcher

import (
	"fmt"
	"os"

	"github.com/fsnotify/fsnotify"
)

type Event struct {
	Path     string
	Info     os.FileInfo
	Type     EventType
	FileData []byte
}

func (e Event) String() string {
	infoStr := ""
	if e.Info != nil {
		infoStr = fmt.Sprintf("%d %s", e.Info.Size(), e.Info.Mode())
	}
	return fmt.Sprintf("%s %s %s", e.Path, e.Type, infoStr)
}

// EventType is a proxy to fsnotify.Op
type EventType fsnotify.Op

func (t EventType) String() string {
	return fsnotify.Op(t).String()
}

func (t EventType) IsCreate() bool {
	return t.is(fsnotify.Create)
}

func (t EventType) IsChmod() bool {
	return t.is(fsnotify.Chmod)
}

func (t EventType) IsRemove() bool {
	return t.is(fsnotify.Remove)
}

func (t EventType) IsWrite() bool {
	return t.is(fsnotify.Write)
}

func (t EventType) IsRename() bool {
	return t.is(fsnotify.Rename)
}

func (t EventType) is(op fsnotify.Op) bool {
	return fsnotify.Op(t)&op == op
}

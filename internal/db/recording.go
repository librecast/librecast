package db

import (
	"github.com/rs/xid"

	"gitlab.com/librecast/librecast/internal/recording"
)

type RecordingDB interface {
	GetRecordingSessionById(id xid.ID) (*recording.Session, error)
	PutRecordingSession(session *recording.Session) error
	DeleteRecordingSessionById(id xid.ID) error
	ListRecordingSessions() ([]*recording.Session, error)
}

package db

import (
	"github.com/rs/xid"

	"gitlab.com/librecast/librecast/internal/recording"
)

var (
	recordingSessionsBucket = []byte("recording_sessions")
)

func (b *Bbolt) GetRecordingSessionById(id xid.ID) (*recording.Session, error) {
	result, err := b.Get(recordingSessionsBucket, id.Bytes())
	if err != nil || result == nil {
		return nil, err
	}
	var session recording.Session
	return &session, unmarshalStruct(result, &session)
}

func (b *Bbolt) PutRecordingSession(session *recording.Session) error {
	data, err := marshalStruct(session)
	if err != nil {
		return err
	}
	return b.Put(recordingSessionsBucket, session.ID.Bytes(), data)
}

func (b *Bbolt) DeleteRecordingSessionById(id xid.ID) error {
	return b.Delete(recordingSessionsBucket, id.Bytes())
}

func (b *Bbolt) ListRecordingSessions() (sessions []*recording.Session, err error) {
	if err := b.ForEach(recordingSessionsBucket, func(k, v []byte) error {
		var session recording.Session
		if err := unmarshalStruct(v, &session); err != nil {
			return err
		}

		sessions = append(sessions, &session)
		return nil
	}); err != nil {
		return nil, err
	}

	return
}

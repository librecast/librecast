package db

import (
	"go.etcd.io/bbolt"
)

type Bbolt struct {
	db *bbolt.DB
}

func NewBbolt(path string, options *bbolt.Options) (DB, error) {
	// mode = -rw-rw----
	db, err := bbolt.Open(path, 0660, options)
	if err != nil {
		return nil, err
	}

	return &Bbolt{db: db}, db.Update(func(tx *bbolt.Tx) error {
		// ensure all buckets are created here
		if _, err := tx.CreateBucketIfNotExists(recordingSessionsBucket); err != nil {
			return err
		}

		return nil
	})
}

func (b *Bbolt) Get(bucket, key []byte) (dst []byte, err error) {
	err = b.db.View(func(tx *bbolt.Tx) error {
		result := tx.Bucket(bucket).Get(key)
		dst = make([]byte, len(result))
		copy(dst, result)
		return nil
	})
	return
}

func (b *Bbolt) Put(bucket, key, value []byte) error {
	return b.db.Update(func(tx *bbolt.Tx) error {
		return tx.Bucket(bucket).Put(key, value)
	})
}

func (b *Bbolt) Delete(bucket, key []byte) error {
	return b.db.Update(func(tx *bbolt.Tx) error {
		return tx.Bucket(bucket).Delete(key)
	})
}

func (b *Bbolt) ForEach(bucket []byte, fn func(k, v []byte) error) error {
	return b.db.View(func(tx *bbolt.Tx) error {
		return tx.Bucket(bucket).ForEach(fn)
	})
}

var _ DB = (*Bbolt)(nil)

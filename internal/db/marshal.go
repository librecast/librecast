package db

import "github.com/vmihailenco/msgpack/v5"

func marshalStruct(i interface{}) ([]byte, error) {
	return msgpack.Marshal(i)
}

func unmarshalStruct(data []byte, out interface{}) error {
	return msgpack.Unmarshal(data, out)
}

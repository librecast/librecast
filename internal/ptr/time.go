package ptr

import "time"

func NewTime(t time.Time) *time.Time {
	return &t
}

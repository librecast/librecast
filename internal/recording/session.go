package recording

import (
	"fmt"
	"time"

	"github.com/rs/xid"
)

type Session struct {
	// ID is a globally unique identifier for the Session
	ID xid.ID
	// Name is a human readable name of the Session
	Name string
	// WatchPath is the local file path to watch for changes
	WatchPath string
	// GitOutputPath is the local file path where changes are pushed to git
	GitOutputPath string
	// VideoOutputPath is the local file path where video files are saved
	VideoOutputPath string
	// CreatedAt is when the Session was created, it does not mean recording started
	CreatedAt time.Time
	// StartedRecordingAt is the time when the Session started to be recorded
	StartedRecordingAt *time.Time
	// EndedRecordingAt is the time when the Session finished being recorded
	EndedRecordingAt *time.Time
}

func (s Session) String() string {
	return fmt.Sprintf("Session:%s %q", s.ID, s.Name)
}

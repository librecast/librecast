package config

import (
	"os"
	"path"

	"github.com/spf13/viper"
)

type Config struct {
	DataDir string
}

func (c Config) DbPath() string {
	return path.Join(c.DataDir, "bbolt.db")
}

func (c Config) GitReposPath() string {
	return path.Join(c.DataDir, "git")
}

func Read() (Config, error) {
	home, err := os.UserHomeDir()
	if err != nil {
		return Config{}, err
	}

	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath(path.Join(home, ".config/librecast"))
	viper.AddConfigPath(".")

	viper.SetDefault("DataDir", path.Join(home, ".local/share/librecast"))

	if err := viper.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			// Config file not found; ignore error if desired
			// TODO: Log
		} else {
			return Config{}, err
		}
	}

	return Config{
		DataDir: viper.GetString("DataDir"),
	}, err
}

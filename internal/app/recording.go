package app

import (
	"os"
	"path/filepath"
	"time"

	"gitlab.com/librecast/librecast/internal/services/repowatcher"

	"gitlab.com/librecast/librecast/internal/watcher"

	"gitlab.com/librecast/librecast/internal/ptr"

	"github.com/rs/xid"

	"gitlab.com/librecast/librecast/internal/recording"
)

const (
	gitDataDirectoryPermissions   = 0700
	videoDataDirectoryPermissions = 0700
)

type CreateRecordingSessionInput struct {
	Name      string
	WatchPath string
}

func (a *App) CreateRecordingSession(input CreateRecordingSessionInput) (*recording.Session, error) {
	session := &recording.Session{
		ID:        xid.New(),
		Name:      input.Name,
		WatchPath: input.WatchPath,
		CreatedAt: time.Now(),
	}

	if err := a.setRecordingSessionGitOutputPath(session); err != nil {
		return nil, err
	}
	if err := a.setRecordingSessionVideoOutputPath(session); err != nil {
		return nil, err
	}

	if err := a.db.PutRecordingSession(session); err != nil {
		return nil, err
	}

	return session, nil
}

func (a *App) StartRecordingSession(sessionID xid.ID) (*recording.Session, error) {
	session, err := a.db.GetRecordingSessionById(sessionID)
	if err != nil || session == nil {
		return nil, err
	}
	if err := a.setRecordingInProgress(sessionID); err != nil {
		return nil, err
	}
	defer func(sessionID xid.ID) {
		// cleanup in case of error
		if r := recover(); r != nil {
			a.clearRecordingInProgress(sessionID)
		}
	}(sessionID)

	repo, err := a.gitSvc.SetupRepository(session.WatchPath, session.GitOutputPath)
	if err != nil {
		a.clearRecordingInProgress(sessionID)
		return nil, err
	}

	w, err := watcher.NewFsNotify()
	if err != nil {
		a.clearRecordingInProgress(sessionID)
		return nil, err
	}

	rw := repowatcher.New(repo, w, a.recordingBucketSize)
	go rw.Watch(a.recordingCtx, a.log.WithField("session_id", sessionID))

	session.StartedRecordingAt = ptr.NewTime(time.Now())
	if err := a.db.PutRecordingSession(session); err != nil {
		return nil, err
	}
	return session, nil
}

func (a *App) StopRecordingInProgress(sessionID xid.ID) (*recording.Session, error) {
	session, err := a.db.GetRecordingSessionById(sessionID)
	if err != nil {
		return nil, err
	}

	a.recordingInProgressMu.Lock()
	a.cancelRecordingCtx()

	session.EndedRecordingAt = ptr.NewTime(time.Now())
	if err := a.db.PutRecordingSession(session); err != nil {
		a.recordingInProgressMu.Unlock()
		return nil, err
	}

	a.recordingInProgressMu.Unlock()
	return session, nil
}

func (a *App) setRecordingSessionGitOutputPath(session *recording.Session) error {
	dir, err := a.appDataLocation()
	if err != nil {
		return err
	}

	session.GitOutputPath = filepath.Join(dir, session.ID.String(), "git_data")
	return os.MkdirAll(session.GitOutputPath, gitDataDirectoryPermissions)
}

func (a *App) setRecordingSessionVideoOutputPath(session *recording.Session) error {
	dir, err := a.appDataLocation()
	if err != nil {
		return err
	}

	session.VideoOutputPath = filepath.Join(dir, session.ID.String(), "video_data")
	return os.MkdirAll(session.VideoOutputPath, videoDataDirectoryPermissions)
}

package app

import (
	"context"
	"errors"
	"sync"
	"time"

	"github.com/sirupsen/logrus"

	"github.com/rs/xid"

	"gitlab.com/librecast/librecast/internal/db"
	"gitlab.com/librecast/librecast/internal/services/git"
)

type App struct {
	db     db.DB
	gitSvc *git.Service

	log logrus.FieldLogger

	recordingBucketSize   time.Duration
	recordingInProgressMu sync.Mutex
	recordingInProgress   bool
	recordingInProgressID xid.ID
	recordingCtx          context.Context
	cancelRecordingCtx    context.CancelFunc
}

func New(db db.DB, gitSvc *git.Service, recordingBucketSize time.Duration, logger logrus.FieldLogger) *App {
	return &App{
		db:                  db,
		gitSvc:              gitSvc,
		recordingBucketSize: recordingBucketSize,
		log:                 logger,
	}
}

func (a *App) setRecordingInProgress(sessionID xid.ID) error {
	a.recordingInProgressMu.Lock()
	defer a.recordingInProgressMu.Unlock()

	if a.recordingInProgress {
		return ErrRecordingAlreadyInProgress
	}

	a.recordingInProgress = true
	a.recordingInProgressID = sessionID
	a.recordingCtx, a.cancelRecordingCtx = context.WithCancel(context.Background())
	return nil
}

func (a *App) clearRecordingInProgress(sessionID xid.ID) {
	a.recordingInProgressMu.Lock()
	defer a.recordingInProgressMu.Unlock()

	if !a.recordingInProgress || a.recordingInProgressID.IsNil() {
		return
	}
	if sessionID.Compare(a.recordingInProgressID) == 0 {
		return
	}

	a.recordingInProgress = false
	a.recordingInProgressID = xid.NilID()
	a.recordingCtx = nil
	a.cancelRecordingCtx = nil
}

var ErrRecordingAlreadyInProgress = errors.New("a recording is already in progress")

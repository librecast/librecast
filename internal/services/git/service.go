package git

import (
	"os"
	"path/filepath"
	"strings"

	"golang.org/x/crypto/openpgp"

	"github.com/monochromegane/go-gitignore"
	"github.com/otiai10/copy"
)

type Service struct {
	authorSignature    *Signature
	committerSignature *Signature
	signKey            *openpgp.Entity
}

// New creates a new Service. The 2 Signature and openpgp.Entity will be passed to
// a repository when it is created. Generally the authorSignature should always be passed,
// otherwise the local git config will be used.
func New(authorSignature *Signature, committerSignature *Signature, signKey *openpgp.Entity) *Service {
	return &Service{
		authorSignature:    authorSignature,
		committerSignature: committerSignature,
		signKey:            signKey,
	}
}

func (s *Service) SetupRepository(srcPath, outputPath string) (*Repository, error) {
	if err := makeCopy(srcPath, outputPath); err != nil {
		return nil, err
	}

	r := &Repository{
		repoPath: outputPath,
		srcPath:  srcPath,

		authorSignature:    s.authorSignature,
		committerSignature: s.committerSignature,
		signKey:            s.signKey,
	}
	return r, r.init()
}

func makeCopy(path string, outputPath string) error {
	var ignorer gitignore.IgnoreMatcher

	gitIgnorePath := filepath.Join(path, ".gitignore")
	if _, err := os.Stat(gitIgnorePath); err == nil {
		ignore, err := gitignore.NewGitIgnore(gitIgnorePath)
		if err != nil {
			return err
		}
		ignorer = ignore
	}

	return copy.Copy(path, outputPath, copy.Options{
		Skip: func(src string) (bool, error) {
			// src is a relative path
			if strings.HasPrefix(src, ".git") {
				return true, nil
			}
			if ignorer != nil {
				info, err := os.Stat(src)
				if err != nil {
					return false, err
				}

				return ignorer.Match(src, info.IsDir()), nil
			}

			return false, nil
		},
	})
}

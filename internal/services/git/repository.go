package git

import (
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"time"

	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing/object"
	"golang.org/x/crypto/openpgp"

	"gitlab.com/librecast/librecast/internal/services/haiku"
	"gitlab.com/librecast/librecast/internal/watcher"
)

type Signature struct {
	Name  string
	Email string
}

func (s Signature) toObjectSig() *object.Signature {
	return &object.Signature{
		Name:  s.Name,
		Email: s.Email,
		When:  time.Now(),
	}
}

type Repository struct {
	r *git.Repository

	authorSignature    *Signature
	committerSignature *Signature
	signKey            *openpgp.Entity

	srcPath  string
	repoPath string
}

func (r *Repository) MakeChanges(events []watcher.Event) error {
	if len(events) == 0 {
		return nil
	}

	for _, event := range events {
		var fn func(watcher.Event) error

		switch {
		case event.Type.IsCreate():
			fn = r.doCreate
		case event.Type.IsChmod():
			fn = r.doChmod
		case event.Type.IsRemove():
			fn = r.doRemove
		case event.Type.IsRename():
			fn = r.doRename
		case event.Type.IsWrite():
			fn = r.doWrite
		default:
			continue
		}

		if err := fn(event); err != nil {
			return err
		}
	}

	return r.addAndCommitAll(haiku.New())
}

func (r *Repository) RepoPath() string {
	return r.repoPath
}

func (r *Repository) SrcPath() string {
	return r.srcPath
}

func (r *Repository) init() (err error) {
	r.r, err = git.PlainInit(r.repoPath, false)
	if err != nil {
		return err
	}

	return r.addAndCommitAll("Initial setup")
}

func (r *Repository) addAndCommitAll(message string) error {
	w, err := r.r.Worktree()
	if err != nil {
		return err
	}

	status, err := w.Status()
	if err != nil {
		return err
	}

	if status.IsClean() {
		// nothing to commit
		return nil
	}

	if err := w.AddWithOptions(&git.AddOptions{All: true}); err != nil {
		return err
	}

	opts := &git.CommitOptions{SignKey: r.signKey}
	if r.authorSignature != nil {
		opts.Author = r.authorSignature.toObjectSig()
	}
	if r.committerSignature != nil {
		opts.Committer = r.committerSignature.toObjectSig()
	}

	_, err = w.Commit(message, opts)
	return err
}

func (r *Repository) doCreate(event watcher.Event) error {
	f, err := os.Create(r.getPath(event))
	if err != nil {
		return err
	}

	if _, err := f.Write(event.FileData); err != nil {
		return err
	}

	return f.Chmod(event.Info.Mode())
}

func (r *Repository) doChmod(event watcher.Event) error {
	return os.Chmod(r.getPath(event), event.Info.Mode())
}

func (r *Repository) doRemove(event watcher.Event) error {
	return os.Remove(r.getPath(event))
}

func (r *Repository) doRename(event watcher.Event) error {
	// TODO: how to handle this?
	return fmt.Errorf("unable to handle rename")
}

func (r *Repository) doWrite(event watcher.Event) error {
	return ioutil.WriteFile(r.getPath(event), event.FileData, event.Info.Mode())
}

func (r *Repository) getPath(event watcher.Event) string {
	evPath := event.Path

	if path.IsAbs(evPath) {
		// make the absolute path relative
		var err error
		evPath, err = filepath.Rel(r.srcPath, evPath)
		if err != nil {
			panic("cannot get relative event path: " + err.Error())
		}
	}

	// return a relative path
	return filepath.Join(r.repoPath, evPath)
}

package haiku

import (
	"time"

	"github.com/yelinaung/go-haikunator"
)

var haiku = haikunator.New(time.Now().UTC().UnixNano())

func New() string {
	return haiku.Haikunate()
}

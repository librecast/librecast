package repowatcher

import (
	"context"
	"sync"
	"time"

	"gitlab.com/librecast/librecast/internal/services/git"
	"gitlab.com/librecast/librecast/internal/watcher"
)

type Service struct {
	repo *git.Repository
	w    watcher.Watcher

	changeQueue []watcher.Event
	m           sync.Mutex

	bucketSize time.Duration
}

func New(repo *git.Repository, w watcher.Watcher, bucketSize time.Duration) *Service {
	return &Service{repo: repo, w: w, bucketSize: bucketSize}
}

func (s *Service) Watch(ctx context.Context, log Logger) {
	ticker := time.NewTicker(s.bucketSize)
	events, errors := s.w.Watch(ctx, s.repo.SrcPath())

	for {
		select {
		case event, ok := <-events:
			if !ok {
				return
			}

			log.Debugf("got event %s -> %s", event.Type.String(), event.Path)

			s.m.Lock()
			s.changeQueue = append(s.changeQueue, event)
			s.m.Unlock()
		case err, ok := <-errors:
			if !ok {
				return
			}

			log.Errorf("error from watcher: %s", err)
		case <-ticker.C:
			s.m.Lock()
			localChanges := make([]watcher.Event, len(s.changeQueue))
			copy(localChanges, s.changeQueue)
			s.changeQueue = nil
			s.m.Unlock()

			log.Debugf("writing %d local change(s) to git as one commit", len(localChanges))

			if err := s.repo.MakeChanges(localChanges); err != nil {
				log.Errorf("unable to make local changes in git repo: %s", err)
			}
		case <-ctx.Done():
			if err := s.w.Close(); err != nil {
				log.Errorf("unable to close watcher: %S", err)
			}

			return
		}
	}
}

type Logger interface {
	Debugf(format string, args ...interface{})
	Errorf(format string, args ...interface{})
	Infof(format string, args ...interface{})
}

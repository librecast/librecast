package api

import (
	"net/http"
	"strings"

	"github.com/go-chi/chi"
)

func NewServer() *http.Server {
	r := chi.NewRouter()

	// TODO: setup files for ../frontend directory
	// Svelte only has 3 files: build/index.html, build/bundle.js, build/bundle.css
	// Plus any assets in public. I think this is the result of rollup.
	FileServer(r, "/", nil)

	r.Route("/api", func(r chi.Router) {
		r.Get("/ping", func(w http.ResponseWriter, r *http.Request) {
			_, _ = w.Write([]byte(`{"ok": true}`))
		})
	})

	srv := &http.Server{
		Addr:    ":4242",
		Handler: r,
		// TODO: other fields like timeouts
	}

	return srv
}

// FileServer conveniently sets up a http.FileServer handler to serve
// static files from a http.FileSystem.
func FileServer(r chi.Router, path string, root http.FileSystem) {
	if strings.ContainsAny(path, "{}*") {
		panic("FileServer does not permit any URL parameters.")
	}

	if path != "/" && path[len(path)-1] != '/' {
		r.Get(path, http.RedirectHandler(path+"/", 301).ServeHTTP)
		path += "/"
	}
	path += "*"

	r.Get(path, func(w http.ResponseWriter, r *http.Request) {
		rctx := chi.RouteContext(r.Context())
		pathPrefix := strings.TrimSuffix(rctx.RoutePattern(), "/*")
		fs := http.StripPrefix(pathPrefix, http.FileServer(root))
		fs.ServeHTTP(w, r)
	})
}
